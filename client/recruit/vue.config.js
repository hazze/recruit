const path = require('path');

module.exports = {
	outputDir: path.resolve(__dirname, "./../../server/dist/public/"),
	pluginOptions: {
		i18n: {
			locale: 'en',
			fallbackLocale: 'en',
			localeDir: 'lang',
			enableInSFC: false
		}
	}
}
