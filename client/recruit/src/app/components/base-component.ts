import Vue from 'vue';
import Component from 'vue-class-component';
import { TranslateResult } from 'vue-i18n';

@Component({})
export default class BaseComponent extends Vue {
	translate(key: string, options?: any): TranslateResult {
        return this.$i18n.t(key, options) || "";
	}
	
	isAdmin(): boolean {
		if (this.$store.state.authentication.user && this.$store.state.authentication.user.admin === true) {
			return true;
		}
		return false;
	}

	isLoggedIn(): boolean {
		if (this.$store.state.authentication.status.loggedIn === true) {
			return true;
		}
		return false;
	}
}
