import BaseComponent from '../base-component';
import { Component, Watch } from 'vue-property-decorator';
import DataService from '../../../services/data-service';
import { Applicant, Competence } from '@/models';



@Component({})
export default class ApplicationsComponent extends BaseComponent {
	headers: any[] = [];
	expand: boolean = false;
	dialog: boolean = false;
	applicants: Applicant[] = [];
	unfilteredApplicants: Applicant[] = [];
	selectedApplicant = <Applicant> {};
	originalApplicant = <Applicant> {};
	search: string = "";
	competences: Competence[] = [];
	compName: string = '';
	date1: any = '';
	date2: any = '';
	filteredApplicants: Applicant[] = [];
	applyResponse: any;
	handlingA: boolean = false;
	handlingR: boolean = false;
	successSnackbar: boolean = false;
	errorSnackbar: boolean = false;
	valErrorSnackbar: boolean = false;

	@Watch('$locale')
	localeWatcher() {
		this.updateTableHeaders();
		DataService.getAllCompetences(this.$i18n.locale)
			.then(y => {
				this.competences = y.data;
		});
	}


	mounted() {
		this.updateTableHeaders();
		this.getAllApplicants();
		DataService.getAllCompetences(this.$i18n.locale)
			.then(y => {
				this.competences = y.data;
		});
	}

	updateTableHeaders() {
		this.headers = [
			{ text: this.translate('Status') },
			{ text: this.translate('Name'), value: 'name' },
			{ text: this.translate('Surname'), value: 'surname' },
			{ text: this.translate('SSN'), value: 'ssn' },
			{ text: this.translate('EMail'), value: 'email' },
			{ text: this.translate('Experience'), value: 'experience.item.type' },
			{ text: this.translate('Availability'), value: 'availability' }
		];
	}

	getAllApplicants(){
		DataService.getAllApplicants()
			.then(x => {
				this.applicants = x.data.filter(y => y.availability != null);
				this.unfilteredApplicants = this.applicants;
			});
	}

	/**
	 * Open the selected application
	 * @param row the applicant who created the application
	 */
	openApplication(row: any) {
		this.originalApplicant = Object.assign({}, row);
		this.selectedApplicant = row;
		this.dialog = true;
	}

	/**
	 * This function will create a object that represent a accepted application and sent it to handleApplication in the dataservice class.
	 * It will also tell the view to generate notifications notifying the user if the request succeed or failed.
	 */
	acceptApplication() {
		const a: any = Object.assign({}, <any> this.applyResponse);
		a.handledBy = this.$store.state.authentication.user.username;
		a.status = "Accepted";
		a.applicant = this.selectedApplicant._id;
		this.handlingA = true;
		DataService.handleApplication(a, this.originalApplicant)
		.then( x => {
			if (x.status === 200) {
				this.getAllApplicants();
				this.handlingA = false;
				this.successSnackbar = true;
				this.dialog = false;
			}
			else if (x.status === 400) {
				this.valErrorSnackbar = true;
				this.handlingA = false;
			}
			else{
				this.handlingA = false;
				this.errorSnackbar = true;
			}
		});
	}

	/**
	 * This function will create a object that represent a rejected application and sent it to handleApplication in the dataservice class.
	 * It will also tell the view to generate notifications notifying the user if the request succeed or failed.
	 */
	rejectApplication() {
		const a: any = Object.assign({}, <any> this.applyResponse);
		a.handledBy = this.$store.state.authentication.user.username;
		a.status = "Rejected";
		a.applicant = this.selectedApplicant._id;
		this.handlingR = true;
		DataService.handleApplication(a, this.originalApplicant)
			.then( x => {
				if (x.status === 200) {
					this.getAllApplicants();
					this.handlingR = false;
					this.successSnackbar = true;
					this.dialog = false;
				} 
				else if (x.status === 400) {
					this.valErrorSnackbar = true;
					this.handlingR = false;
				}
				else{
					this.handlingR = false;
					this.errorSnackbar = true;
				}
			});
	}
	/**
	 * Filters the applicant array according to the the variables compName, date1 and date2. 
	 * The function uses find() to determine if an applicant has the value we want. 
	 * The applicants list is updated and the old list is saved to another variable to be used later. 
	 */
	saveFiltersUpdateList() {		
		this.filteredApplicants = this.applicants.filter(x => {
			if (this.compName !== '' && this.date1 !== '' && this.date2 !== ''){
				
				if (x.experience !== null && x.availability !== null){
					if (x.experience.find(y => y.type === this.compName) && 
						x.availability.find(y => (new Date(y.from_date) >= new Date(this.date1) && new Date(y.to_date) <= new Date(this.date2)))){
							return true;
					}
				}
			}
			else if (this.compName !== '' && this.date1 === '' && this.date2 === ''){
				if (x.experience !== null){
					if (x.experience.find(z => z.type === this.compName)){
						return true;
					}
				}
			}
			else if (this.compName === '' && this.date1 !== '' || this.date2 !== ''){
				if (x.availability !== null){
					if (x.availability.find(z => (new Date(z.from_date) >= new Date(this.date1) || new Date(z.to_date) <= new Date(this.date2)))){
						return true;
					}
				}
			}
		});

		this.applicants = this.filteredApplicants;
	}
	/**
	 * Clears all filters and resets the applicants list with the original. 
	 */
	clearFilter() {
		this.applicants = this.unfilteredApplicants;
		this.compName = '';
		this.date1 = '';
		this.date2 = '';
	}
}
