import BaseComponent from '../base-component';
import { Component, Watch } from 'vue-property-decorator';
import { Applicant, Experience, Availability, Competence } from '@/models';
import dataService from '@/services/data-service';


@Component({})
export default class ApplyComponent extends BaseComponent {
	menu1: boolean = false;
	menu2: boolean = false;
	date: string = new Date().toISOString().substr(0, 10);
	competences: string[] = [];
	selectedCompenteces: Experience[] = [
		{ type: "", years: 0 }
	];
	selectedAvailabilities: Availability[] = [
		{ from_date: this.date, to_date: this.date }
	];
	currentUser: Applicant = {
		_id: "",
		person_id: 0,
		name: "",
		surname: "",
		email: "",
		ssn: "",
		availability: [],
		experience: [],
		password: "",
		application: null,
		submissionDate: ""
	};
	loading: boolean = false;
	submitting: boolean = false;
	submitComplete: boolean = false;
	submissionError: boolean = false;
	valErrorSnackbar: boolean = false;
	application: any;
	disableApply: boolean = false;

	mounted() {
		this.loading = true;
		dataService.getCurrentUserInfo(this.$store.state.authentication.user.username)
			.then(x => {
				dataService.getAllCompetences(this.$i18n.locale)
					.then(y => {
						this.competences = y.data.map(z => z.name);
					});
				this.currentUser = x.data;
				this.loading = false;
			});
	}

	/**
	 * Adds a competence to selectedCompenteces 
	 */
	pushCompetence() {
		this.selectedCompenteces.push({ type: "", years: 0 });
	}

	/**
	 * Adds a Availabilitie to selectedAvailabilities
	 */
	pushAvailability() {
		this.selectedAvailabilities.push({ from_date: new Date().toISOString().substr(0, 10), to_date: new Date().toISOString().substr(0, 10) });
	}

	/**
	 * Deletes a competence from selectedCompenteces 
	 * @param index the index of the compitence that shoul be deleted
	 */
	deleteCompetence(index: number) {
		this.selectedCompenteces.splice(index, 1);
	}

	/**
	 * Delete a availability from selectedAvailabilities
	 * @param index 
	 */
	deleteAvailability(index: number) {
		this.selectedAvailabilities.splice(index, 1);
	}

	disableApplyBtn(){
		if (this.selectedAvailabilities.length === 0){
			this.disableApply = true;
		}
		else {
			this.disableApply = false;
		}
	}
	/**
	 * This watcher watches the selected availabilities object for any changes and triggers the disableApplyBtn function upon any change. 
	 */
	@Watch("selectedAvailabilities", {deep: true})
	applyWatcher() {
		this.disableApplyBtn();
	}

	/**
	 * Creates an application object with ssn, availability and experience and send it to the dataservice class, 
	 * it also handle some view showing if the request was successfull of failed.
	 */
	addApplication() {
		const a: any = Object.assign({}, <any> this.application);
		a.id = this.currentUser._id;
		a.availability = this.selectedAvailabilities;
		a.experience = this.selectedCompenteces;

		this.submitting = true;
		this.submitComplete = false;
		this.submissionError = false;
		dataService.addApplication(a)
		.then(x => {
			if (x.status === 200) {
				this.submitComplete = true;
			}
			else{
				this.submissionError = true;
			}
			this.submitting = false;
		}).catch(err => {
			this.valErrorSnackbar = true;
		});
	}
}
