import BaseComponent from '../base-component';
import Component from "vue-class-component";
import passwordhash from "password-hash";
import { Prop, Watch } from 'vue-property-decorator';
import { Applicant, ValidationError } from '@/models';
import dataService from '@/services/data-service';
import { ValidationRule } from '@/models/ValidationRules';


@Component({})
export default class RegisterDialogComponent extends BaseComponent {
	@Prop()
	registerDialogVisible!: boolean;

	user = <Applicant> {};
	confirmPassword: string = "";
	loading: boolean = false;
	
	successSnackbar: boolean = false;
	errorSnackbar: boolean = false;
	valErrorSnackbar: boolean = false;

	disableReg: boolean = true;
	complexErrors: ValidationError[] = [];
	
	hideDialog() {
		this.user = <Applicant> {};
		this.confirmPassword = "";
		this.$emit("hideDialog");
	}

	backToLogin() {
		this.user = <Applicant> {};
		this.confirmPassword = "";
		this.$emit("hideDialog");
		this.$emit("toLogin");
	}

	noErrors() {
		if (this.complexErrors.find(x => x !== null)){
			this.disableReg = true;
		}
		else {
			this.disableReg = false;
		}
	}

	required(errors: ValidationError[], value: any, key: string, ref: string) {
		if (value == null || value === "" || value.toString().length === 0) {
			if (!errors.find(x => (x.key === key))) {
				errors.push(<ValidationError> { key, error: ref, refId: ref, rule: ValidationRule.Required});
			}
		}
		else {
			const ver: any = errors.find(x => x.key === key);
			if (ver !== undefined){
				const index = errors.indexOf(ver);
				errors.splice(index);
			}
		}
	}

	isPasswordValidAndRepeated(errors: ValidationError[], value: string, key: string, ref: string) {
		if (value == null || value === "" || value !== this.user.password ){
			
			if (!errors.find(x => (x.key === key))) {
				errors.push(<ValidationError> { key, error: ref, refId: ref, rule: ValidationRule.Required});
			}
		}
		else {
			const ver: any = errors.find(x => x.key === key);
			if (ver !== undefined){
				const index = errors.indexOf(ver);
				errors.splice(index);
			}
		}
	}
	/**
	 * Checks the validity of email or ssn using pattern matching. 
	 * @param errors The array holding the validation errors. 
	 * @param value The value of the input field
	 * @param key a key representing the input field
	 * @param ref a reference to what the error is
	 */
	isEmailOrSSNValid(errors: ValidationError[], value: any, key: string, ref: string) {
		const mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const ssn = /^(19|20)?(\d{6}([-+]|\s)\d{4}|(?!19|20)\d{10})$/;
	
		if (key === "email"){
				if (value == null || value === "" || value.toString().length === 0 || !mail.test(value.toLowerCase())){
					if (!errors.find(x => x.key === key)){
						errors.push(<ValidationError> { key, error: ref, refId: ref, rule: ValidationRule.Email});
					}
				}
				else {
					const ver: any = errors.find(x => x.key === key);
					if (ver !== undefined){
						const index = errors.indexOf(ver);
						errors.splice(index);
					}
				}
			}
		else if (key === "ssn"){
			if (value == null || value === "" || value.toString().length === 0 || !ssn.test(value)) {
				if (!errors.find(x => x.key === key)){
					errors.push(<ValidationError> { key, error: ref, refId: ref, rule: ValidationRule.Email});
				}
			}
			else {
				const ver: any = errors.find(x => x.key === key);
				if (ver !== undefined){
					const index = errors.indexOf(ver);
					errors.splice(index);
				}
			}
		}	
	}

	

	validateUser(userToBeValidated: Applicant, complexErrors: ValidationError[]) {
		this.required(this.complexErrors, userToBeValidated.name, "name", "nameRequired");
		this.required(this.complexErrors, userToBeValidated.surname, "sur", "surnameRequired");
		this.isEmailOrSSNValid(this.complexErrors, userToBeValidated.email, "email", "nonValidEmail");
		this.isEmailOrSSNValid(this.complexErrors, userToBeValidated.ssn, "ssn", "nonValidSSN");
		this.required(this.complexErrors, userToBeValidated.password, "pw", "passwordRequired");
		this.required(this.complexErrors, this.confirmPassword, "repeat", "repeatPassword");
		this.isPasswordValidAndRepeated(this.complexErrors, this.confirmPassword, "repeat", "repeatPassword");
	}

	/**
	 * Watches the user object for any change i.e. any input on the registration form, 
	 * if there's any change validateUser and noErrors are triggered to check the validity of the input
	 */
	@Watch("user", {deep: true})
	userWatcher() {
		this.validateUser(<Applicant> this.user, this.complexErrors);
		this.noErrors();
	}
	/**
	 * Watches the confirmPassword variable since it is not a part of the user object and thus any change in confirmPassword field. 
	 */
	@Watch("confirmPassword", {deep: true})
	passwordWatcher() {
		this.isPasswordValidAndRepeated(this.complexErrors, this.confirmPassword, "repeat", "repeatPassword");
		this.noErrors();
	}
	
	reformatSSN() {
		if (this.user.ssn.length === 12){
			this.user.ssn =  this.user.ssn.substr(2, 10);
		}
		else if (this.user.ssn.length === 13) {
			this.user.ssn = this.user.ssn.slice(2, 8) + this.user.ssn.slice(9, 13);
		}
		else if (this.user.ssn.length === 11) {
			this.user.ssn = this.user.ssn.slice(0, 6) + this.user.ssn.slice(7, 11);
		}
	}

	getError(errorText: string) {
		if (this.complexErrors.find(x => x.error === errorText)) {
			return true;
		}
	}

	/**
	 * Sends a object of a user that has signed upp to the backend.
	 * The function also hashes the password and set availability and experience to the deffault value
	 */

    registerUser() {
		this.loading = true;
		const u: Applicant = Object.assign({}, <Applicant> this.user);
		u.email = u.email;
		u.password = u.password;
		u.person_id = -1;
		u.availability = null;
		u.experience = null;
		dataService.registerApplicant(u)
			.then(x => {
				this.loading = false;
				if (x.status === 200) {
					this.successSnackbar = true;
					this.backToLogin();
				} 
			})
			.catch(err => {
				this.valErrorSnackbar = true;
				this.loading = false;
			});
    }
}
