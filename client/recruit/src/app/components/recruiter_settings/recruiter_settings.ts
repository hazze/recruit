import BaseComponent from '../base-component';
import { Component } from 'vue-property-decorator';
import dataService from '@/services/data-service';
import { Language } from '../../../models/Language';
import { CompetenceDTO } from '../../../models/CompetenceDTO';


@Component({})
export default class RecruiterSettingsComponent extends BaseComponent {
	comp: string = "";
	competence: CompetenceDTO = {};
	confirm: boolean = false;
	error: boolean = false;
	langs: Language[] = [];
	selectedLang: string = "sv";

	mounted() {
		dataService.getLanguages()
			.then(x => {
				this.langs = x.data;
			});
	}

	saveComp() {
		this.competence = Object.assign(this.competence, { [this.selectedLang]: this.comp });
	}

	/**
	 * Sends a competence to the server to add into the database 
	 */
  addCompetence() {
		dataService.addCompetence(this.competence)
			.then(x => {
				if (x.status === 200) {
					this.competence = {};
					this.comp = "";
				}
				else {
					this.error = true;
				}
		});
  }
}
