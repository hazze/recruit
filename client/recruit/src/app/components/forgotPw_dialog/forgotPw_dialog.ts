import BaseComponent from '../base-component';
import Component from "vue-class-component";
import { Prop } from 'vue-property-decorator';


@Component({})
export default class ForgotPwDialogComponent extends BaseComponent {
	@Prop()
	forgotPwDialogVisible!: boolean;

	email: string = "";

	hideDialog() {
		this.$emit("hideDialog");
    }

  reqPw() {
        // request pw from server
	}
	
	backToLogin() {
		this.email = "";
		this.$emit("hideDialog");
		this.$emit("toLogin");
	}
}
