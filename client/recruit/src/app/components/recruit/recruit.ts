import BaseComponent from '../base-component';
import Component from 'vue-class-component';
import ApplyComponent from '../apply/apply';
import ApplicationsComponent from '../applications/applications';
import LoginDialogComponent from '../login_dialog/login_dialog';
import { NavigationViewsEnum } from '@/models/constants';
import HomeComponent from '../home/home';
import NavigationComponent from '../navigation/navigation';
import RegisterDialogComponent from '../register_dialog/register_dialog';
import ForgotPwDialogComponent from '../forgotPw_dialog/forgotPw_dialog';
import RecruiterSettingsComponent from '../recruiter_settings/recruiter_settings';
import { TranslateResult } from 'vue-i18n';
import { Watch } from 'vue-property-decorator';
import dataService from '@/services/data-service';
import { Language } from '../../../models/Language';

@Component({
	components: {
		HomeComponent,
		ApplyComponent,
		ApplicationsComponent,
		LoginDialogComponent,
		NavigationComponent,
		RegisterDialogComponent,
		ForgotPwDialogComponent,
		RecruiterSettingsComponent
	}
})
export default class RecruitComponent extends BaseComponent {
	navigationViews = NavigationViewsEnum;
	drawer: any = null;
	loginDialogVisible: boolean = false;
	registerDialogVisible: boolean = false;
	forgotPwDialogVisible: boolean = false;
	activeView: NavigationViewsEnum = NavigationViewsEnum.Home;
	langs: Language[] = [];
	langDropdown: boolean = false;
	on: any = null;

	mounted() {
		this.updatedLangs();
	}

	@Watch('$locale')
	localeWatcher() {
		this.updatedLangs();
	}

	updatedLangs() {
		dataService.getLanguages()
			.then(x => {
				this.langs = x.data;
			});
	}

	setActiveView(view: NavigationViewsEnum) {
		this.activeView = view;
	}

	openRegCloseLog() {
		this.loginDialogVisible = false;
		this.registerDialogVisible = true;
	}

	openForgotPwCloselog() {
		this.loginDialogVisible = false;
		this.forgotPwDialogVisible = true;
	}

	logout() {
		this.$store.dispatch('authLogout', {});
		this.activeView = NavigationViewsEnum.Home;
		this.$router.push('/');
	}
}

