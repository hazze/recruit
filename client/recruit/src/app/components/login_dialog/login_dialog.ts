import BaseComponent from '../base-component';
import Component from "vue-class-component";
import { Prop } from 'vue-property-decorator';
import { User } from '../../../models';
import { mapGetters } from 'vuex';


@Component({
	computed: mapGetters({
		alertError: 'getAlertType'
	})
})
export default class LoginDialogComponent extends BaseComponent {
	@Prop()
	loginDialogVisible!: boolean;

	alertError!: any;

	user: User = { username: "", password: "" };
	successSnackbar: boolean = false;
	errorAlert: boolean = false;
	successAlert: boolean = false;

	hideDialog() {
		this.$emit("hideDialog");
	}

	changeToRegForm() {
		this.$emit('changeToRegForm');
	}

	changeToForgotPwForm() {
		this.$emit('changeToForgotPwForm');
	}

	/**
	 * This funktion checks if the user tries to login with a with a email or with a username. If its a email it will be logged in as a applicant
	 * and if its a username it will be logged in as a recruiter (Admin). the function also handles success and error message to the view.
	 */
	async login() {
		this.$store.dispatch('alertClear', "");
		this.errorAlert = false;
		this.successAlert = false;
		const regex: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		const  admin: boolean = regex.test(this.user.username) ? false : true;
		const { username, password } = this.user;
		if (username && password) {
			await this.$store.dispatch('authLogin', { username, password, admin })
			.then(() => {
				if (this.alertError === 'alert-error') {
					this.errorAlert = true;
				}
				else {
					this.successAlert = true;
					setTimeout(() => {
						this.successAlert = false;
						this.hideDialog();
					}, 1000);
				}
			});
		}	
	}
}
