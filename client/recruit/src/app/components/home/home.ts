import BaseComponent from '../base-component';
import Component from 'vue-class-component';
import DataService from '../../../services/data-service';


@Component({})
export default class HomeComponent extends BaseComponent {

	couldNotLoadDB: boolean = false; 

	mounted() {
		this.databaseCheck();
	}

	databaseCheck() {
		DataService.databaseCheck()		
		.then( x => {
			if (x.status !== 200) {
				this.couldNotLoadDB = true;
			}
		})
		.catch(err => {
			this.couldNotLoadDB = true;
		});
	}

	/**
	 * Authorisation Checks
	 */
  goToApply() {
		if (this.isLoggedIn()) {
			this.$emit('applyPage');
		}
		else {
			this.$emit('openLogin');
		}
	}
}
