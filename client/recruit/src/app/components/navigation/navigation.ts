import BaseComponent from '../base-component';
import { Component, Prop} from 'vue-property-decorator';

@Component({})
export default class NavigationComponent extends BaseComponent {
    @Prop()
    drawer!: any;

    closing() {
        this.$emit('closing');
    }
}
