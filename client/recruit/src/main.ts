import Vue from 'vue';
import './plugins/vuetify';
import App from './app/App.vue';
import { router } from './helpers';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { store } from './store/store';
import i18n from './i18n';

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);

Object.defineProperty(Vue.prototype, '$locale', {
    get: () => {
      return i18n.locale;
    },
    set: (locale) => {
      i18n.locale = locale;
    }
  });

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App)
}).$mount('#app');


