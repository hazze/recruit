export interface Availability {
	from_date: string;
	to_date: string;
}
