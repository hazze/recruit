import { TranslateResult } from 'vue-i18n';

export interface Language {
	name: string | TranslateResult;
	code: string;
}
