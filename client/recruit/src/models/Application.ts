export interface Application {
	status: string;
	handledBy: string;
}
