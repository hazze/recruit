import { ValidationRule } from './ValidationRules';

export interface ValidationError {
    key: string;
    error: string;
    refId: string;
    rule: ValidationRule;
}
