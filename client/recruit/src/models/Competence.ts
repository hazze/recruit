export interface Competence {
    comp_id: number;
    name: string;
}
