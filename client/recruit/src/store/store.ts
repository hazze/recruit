import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import * as Cookies from 'js-cookie';
import { MutationTree, ActionTree } from 'vuex';
import { router } from '../helpers/router';
import authenticationService from '../services/authentication-service';

Vue.use(Vuex);

interface State {
	alert: { type: string, message: string };
	authentication: { status: any, user: any };
	users: { all: any };
}

const mutations: MutationTree<State> = {
	alertSuccess: (s, message) => {
		s.alert.type = 'alert-success';
		s.alert.message = message;
	},
	alertError: (s, message) => {
		s.alert.type = 'alert-error';
		s.alert.message = message;
	},
	alertClear: (s) => {
		s.alert.type = '';
		s.alert.message = '';
	},
	authLoginRequest(s, u): any {
		s.authentication.status = { loggedIn: true };
		s.authentication.user = u;
	},
	authLoginSuccess(s, u): any {
		s.authentication.status = { loggedIn: true };
		s.authentication.user = u;
	},
	authLoginFailure(s): any {
		s.authentication.status = {};
		s.authentication.user = null;
	},
	authLogout(s): any {
		s.authentication.status = { loggedIn: false };
		s.authentication.user = null;
	},
	usersGetAllRequest(s): any {
		s.users.all = { loading: true };
	},
	usersGetAllSuccess(s, u): any {
		s.users.all = { items: u };
	},
	usersGetAllFailure(s, error): any {
		s.users.all = { error };
	}
};

const actions: ActionTree<State, any> = {
	alertSuccess({ commit }, message): any {
		commit('alertSuccess', message);
	},
	alertError({ commit }, message): any {
		commit('alertError', message);
	},
	alertClear({ commit }, message): any {
		commit('alertSuccess', message);
	},
	async authLogin({ dispatch, commit }, { username, password, admin }) {
		await authenticationService.login(username, password, admin)
			.then(u => {
				commit('authLoginSuccess', u);
				router.push('/authorized');
			}, e => {
				commit('authLoginFailure', e);
				dispatch('alertError', e, { root: true });
			}
		);
	},
	authLogout({ commit }): any {
		authenticationService.logout();
		commit('authLogout');
	},
	usersGetAll({ commit }): any {
		commit('userGetAllRequest');
		authenticationService.getAll()
			.then(
				u => commit('userGetAllSuccess', u),
				e => commit('userGetAllFailure', e)
			);
	}
};

const state: State = {
	alert: { type: "", message: "" },
	authentication: { status: { loggedIn: false }, user: {}},
	users: { all: {} }
};

export const store = new Vuex.Store<State>({
	state,
	mutations,
	actions,
	getters: {
		getAlert: s => s.alert,
		getAlertType: s => s.alert.type,
		getAlertMessage: s => s.alert.message
	},
	plugins: [
		createPersistedState({
		  getState: (key) => Cookies.getJSON(key),
		  setState: (key, value) => Cookies.set(key, value, { expires: 3, secure: false })
		})
	]
});
