import Vue from 'vue';
import Router from 'vue-router';
import RecruitComponent from '../app/components/recruit/recruit';

Vue.use(Router);

export const router = new Router({
	mode: 'history',
	routes: [
		{ path: '/', name: 'home', component: RecruitComponent },
		{ path: '/authorized', name: 'authorized', component: RecruitComponent },
		{ path: '*', redirect: '/' },
	],
});

router.beforeEach((to, from, next) => {
	const publicPages = ['/'];
	const authRequired = !publicPages.includes(to.path);
	const loggedIn = localStorage.getItem('token');

	if (authRequired && !loggedIn) {
		return next('/');
	}
	next();
});
Vue.use(Router);
