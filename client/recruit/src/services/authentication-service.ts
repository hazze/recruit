import { AuthorizationHeader } from '../helpers';

const api = '/api/';
const authHeader = new AuthorizationHeader();

export default new class AuthenticationService {

	/**
	 * handle request for login and sends it to the correct funktion depending on if its recruiter or applicant
	 * @param username the username/Email of the user trying to login
	 * @param password the password of the user
	 * @param admin a boolean that is true if the user is recruiter
	 */
	async login(username: string, password: string, admin: boolean) {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ username, password })
		};

		if (admin === true) {
			return await fetch(`${api}recruits/login`, requestOptions)
				.then(this.handleResponse)
				.then((token: any) => {
					if (token) {
						localStorage.setItem('token', JSON.stringify(token));
					}
					return token;
				});
		}
		else {
			return await fetch(`${api}applicants/login`, requestOptions)
				.then(this.handleResponse)
				.then((token: any) => {
					if (token) {
						localStorage.setItem('token', JSON.stringify(token));
					}
					return token;
				});
		}
	}

	/**
	 * logging out a user
	 */
	logout() {
		localStorage.removeItem('token');
	}
	
	/**
	 * gets all applicants
	 */
	getAll() {
		const requestOptions = {
			method: 'GET',
			headers: authHeader.authorizationHeader()
		};
	
		return fetch(`${api}applicants/all`, requestOptions).then(this.handleResponse);
	}

	/**
	 * handles the response when tring to login
	 */
	handleResponse(response: any) {
		return response.text().then((text: any) => {
			const data = text && JSON.parse(text);
			if (!response.ok) {
				if (response.status === 401) {
					// auto logout if 401 response returned from api
					this.logout();
					location.reload(true);
				}
	
				const error = (data && data.message) || response.statusText;
				return Promise.reject(error);
			}
	
			return data;
		});
	}
};
