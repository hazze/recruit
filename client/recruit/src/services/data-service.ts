import axios from 'axios';
import { AxiosInstance, AxiosPromise } from 'axios';
import { Applicant, User, Competence, Language } from '../models/';
import { CompetenceDTO } from '../models/CompetenceDTO';

export default new class DataService {

	/**
	 * initialize a Axios instance with predefined serttings
	 */
	initialize(): AxiosInstance {
		let ax: AxiosInstance;
		ax = axios.create({
			baseURL: '/api/',
			params: {},
			headers: {
				'Content-Type': 'application/json',
				// 'Access-Control-Allow-Origin': '*',
				// 'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
			}
		});

		ax.interceptors.response.use(undefined, (error: any) => {
			const code = (error && error.response && error.response.status) ? error.response.status : "";
			const message = (error &&  error.message) ? error.message : "";
			const data = (error && error.response && error.response.data) ? error.response.data : "";
			console.log("HTTP.interceptor", error);
			return Promise.reject(error);
		});

		ax.interceptors.request.use(
			(config) => {
			  const user = localStorage.getItem('token') ? localStorage.getItem('token') : null;
			  const token = JSON.parse(<string> user);
			  if (token) {
				config.headers['Authorization'] = token.token;
			  }
			  return config;
			}, 
			(error) => {
			  return Promise.reject(error);
			}
		);
		return ax;
	}

	/**
	 * get all appplicants
	 */
	getAllApplicants(): AxiosPromise<Applicant[]> {
		const http = this.initialize();
		return http.get('applicants/all');
	}

	/**
	 * gets all competences of the chosen language 
	 * @param lang the chosen languge
	 */
	getAllCompetences(lang: string): AxiosPromise<Competence[]> {
		const http = this.initialize();
		return http.get(`competences/all/${lang}`);
	}

	/**
	 * get the the chosen application by id
	 * @param id the id of the chosen application
	 */
	getCurrentUserInfo(id: string): AxiosPromise<Applicant> {
		const http = this.initialize();
		return http.get(`applicants/byId/${id}`);
	}

	/**
	 * register a new applicant
	 * @param applicant an object with all the userdata that need to be stored in the database
	 */
	registerApplicant(applicant: Applicant): AxiosPromise<Applicant> {
		const http = this.initialize();
		return http.post('applicants/add', JSON.stringify(applicant));
	}

	/**
	 * login a user
	 * @param user an object with user data
	 */
	loginRecruit(user: User): AxiosPromise<User> {
		const http = this.initialize();
		return http.post('recruit/login', JSON.stringify(user));
	}

	/**
	 * adds an application
	 * @param application an object with application data 
	 */
	addApplication(application: any): AxiosPromise<any> {
		const http = this.initialize();
		return http.post('application/add', JSON.stringify(application));
	}

	/**
	 * Adds an application
	 * @param competence the compitence that should be added
	 */
	addCompetence(competence: CompetenceDTO): AxiosPromise<any> {
		const http = this.initialize();
		return http.post(`competences/add/`, JSON.stringify(competence));
	}

	/**
	 * handle an application
	 * @param application the new data that should be added to an application
	 * @param original the old verson of the application
	 */
	handleApplication(application: any, original: Applicant): AxiosPromise<any> {
		const http = this.initialize();
		return http.post('application/handle', JSON.stringify({ application, original }));
	}

	/**
	 * get all languages 
	 */
	getLanguages(): AxiosPromise<Language[]> {
		const http = this.initialize();
		return http.get('languages/all');
	}

	/**
	 * checks if the database is working 
	 */
	databaseCheck(): AxiosPromise<any> {
		const http = this.initialize();
		http.defaults.timeout = 2000;
		return http.get('applicants/databaseCheck');
	}
};
