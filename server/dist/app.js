"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const routes_1 = require("./routes/routes");
const options = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token", "Authorization"],
    credentials: true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    origin: '*',
    preflightContinue: false
};
class App {
    constructor() {
        this.router = new routes_1.Routes();
        this.mongoUrl = 'mongodb+srv://badanka:BurkHund123@cluster0-a2mgm.mongodb.net/test?retryWrites=true';
        this.app = express();
        const expressValidator = require('express-validator');
        this.config();
        this.app.use(cors(options));
        this.app.use(expressValidator({
            errorFormatter: function (param, msg, value) {
                var namespace = param.split('.'), root = namespace.shift(), formParam = root;
                while (namespace.length) {
                    formParam += '[' + namespace.shift() + ']';
                }
                return {
                    param: formParam,
                    msg: msg,
                    value: value
                };
            }
        }));
        this.router.routes(this.app);
        this.mongoSetup();
    }
    config() {
        this.app.use(express.static(path.resolve(__dirname, "./../dist/public/")));
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
    mongoSetup() {
        mongoose.Promise = Promise;
        mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useFindAndModify: false, dbName: "Recruit" })
            .catch(err => {
            console.log('database error:', err);
        });
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map