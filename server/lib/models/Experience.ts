export interface Experience {
	type: string;
	years: number;
}
