export interface Recruit {
	person_id: number;
	name: string;
	surname: string;
	username: string;
	password: string;
}