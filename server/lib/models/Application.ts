export interface Application {
	status: String;
	handledBy: String;
}