import * as mongoose from 'mongoose'

const Schema = mongoose.Schema;

export const RecruitSchema = new Schema({
	id: {
		type: Number,
	},
	name: {
		type: String,
		required: 'Enter a first name'
	},
	surname: {
		type: String,
		required: 'Enter a surname'
	},
	username: {
		type: String
	},
	password: {
		type: String
	}
});