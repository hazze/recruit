import * as mongoose from 'mongoose'

const Schema = mongoose.Schema;

export const CompetenceSchema = new Schema({
	comp_id: {
		type: Number
	},
	nameEn: {
		type: String
	},
	nameSv: {
		type: String
	}
});

