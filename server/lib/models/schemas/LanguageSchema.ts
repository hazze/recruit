import * as mongoose from 'mongoose'

const Schema = mongoose.Schema;

export const LanguageSchema = new Schema({
	name: {
		type: String
	},
	code: {
		type: String
	}
});

