import * as mongoose from 'mongoose'

const Schema = mongoose.Schema;

export const ApplicantSchema = new Schema({
	person_id: {
		type: Number
	},
	name: {
		type: String
	},
	surname: {
		type: String
	},
	ssn: {
		type: String
	},
	email: {
		type: String
	},
	password: {
		type: String
	},
	experience: [{
		type: {
			type: String
		},
		years: {
			type: String
		}
	}],
	availability: [{
		from_date: {
			type: String
		},
		to_date: {
			type: String
		}
	}],
	application: {
		status: {
			type: String
		},
		handledBy: {
			type: String
		},
	},
	submissionDate: {
		type: String
	}
});

