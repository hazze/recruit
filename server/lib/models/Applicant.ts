import { Experience, Availability, Application } from '.';

export interface Applicant {
	person_id: number;
	name: string;
	surname: string;
	ssn: string;
	email: string;
	password: string;
	experience: Experience[] | null;
	availability: Availability[] | null;
	application: Application | null;
	submissionDate: String
}
