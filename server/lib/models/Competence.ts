export interface Competence {
	comp_id: number;
	nameEn: string;
	nameSv: string;
}