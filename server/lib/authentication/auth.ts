import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';

export class Authentication {

	/**
	 * verifies a json web token
	 * @param token the jwt that need to be verified 
	 */
	verifyJWT(token: string) {
		return new Promise((resolve, reject) => {
			jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
				if (err || !decodedToken) {
					return reject(err);
				}
				resolve(decodedToken);
			});
		});
	}

	/**
	 * Create a json web token
	 * @param details an object with current session data
	 */
	createJWT(details: any | null): string {
		if (typeof details !== 'object') {
			details = {};
		}

		if (!details.maxAge || typeof details.maxAge !== 'number') {
			details.maxAge = 3600;
		}

		details.sessionData = _.reduce(details.sessionData || {}, (memo, val, key) => {
			if (typeof val !== 'function' && key !== 'password') {
				memo[key] = val;
			}
			return memo
		}, {});

		let token: string = jwt.sign(
			{ data: details.sessionData },
			process.env.JWT_SECRET, 
			{ expiresIn: details.maxAge, algorithm: 'HS256' }
		);

		return token
	}
}