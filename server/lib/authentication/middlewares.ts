import { Authentication } from './auth';
import { NextFunction } from 'connect';
import { Response, Request } from 'express';


const auth = new Authentication();

export class Middlewares {

	/**
	 * middleware for JWT verification
	 * @param req the token that need to be verified
	 * @param res the response if error
	 * @param next next function to execute
	 */
	verifyJWT_MW(req: Request, res: Response, next: NextFunction) {
		let token = req.headers['authorization'];
		auth.verifyJWT(token)
			.then((decodedToken: any) => {
				next();
			})
			.catch(err => {
				res.status(400);
				res.json({ message: "Invalid auth token provided" });
				res.send();
			});
	}

	/**
	 * creates a JWT
	 * @param details current session data
	 */
	createJWT_MW(details: any): string {
		return auth.createJWT(details);
	}
}