import 'dotenv/config';
import * as express from "express";
import * as path from "path";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as cors from "cors";
import { Routes } from "./routes/routes";


const options: cors.CorsOptions = {
	allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token", "Authorization"],
	credentials: true,
	methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
	origin: '*',
	preflightContinue: false
};

class App {
	public app: express.Application;
	public router: Routes = new Routes();
	public mongoUrl: string = 'mongodb+srv://badanka:BurkHund123@cluster0-a2mgm.mongodb.net/test?retryWrites=true';

    constructor() {
		this.app = express();
		const expressValidator = require('express-validator')
		this.config();
		this.app.use(cors(options));
		this.app.use(expressValidator({
			errorFormatter: function(param, msg, value) {
				var namespace = param.split('.')
				, root    = namespace.shift()
				, formParam = root;
		  
			  while(namespace.length) {
				formParam += '[' + namespace.shift() + ']';
			  }
			  return {
				param : formParam,
				msg   : msg,
				value : value
			  };
			}
		  }));
		this.router.routes(this.app);
		this.mongoSetup();
    }

    private config(): void {
		this.app.use(express.static(path.resolve(__dirname, "./../dist/public/")));
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
	}
	
	private mongoSetup(): void{
        (<any> mongoose).Promise = Promise;
		mongoose.connect(this.mongoUrl, {useNewUrlParser: true, useFindAndModify: false, dbName: "Recruit"})
			.catch(err => {
				console.log('database error:' , err);
			});       
    }
}

export default new App().app;
