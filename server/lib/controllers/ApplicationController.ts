import * as mongoose from "mongoose";
import { Request, Response } from 'express';
import { ApplicantSchema } from '../models/schemas'


const Applicants = mongoose.model('Applicants', ApplicantSchema, 'Applicants');
const { check, validationResult } = require('express-validator/check');

export class ApplicationController {

	/**
	 * This function finds a user by SSN and adds an application to a user (experiens, availability and submision date) and put it in the database.
	 * 
	 * @param req an Object with the user's ssn, the chosen availabilitys and the chosen experienses.
	 * @param res on succes it will return status 200, else it will return the error.
	 */
	addApplication(req: Request, res: Response) {
		const reqBdy: any = req;
		reqBdy.check('ssn').isNumeric().len({ max: 10 });
		reqBdy.check('availability').notEmpty();
		reqBdy.check('experience').notEmpty();
		let errors = reqBdy.validationErrors();

		if(errors) {
			res.sendStatus(406);
		}
		else {
			Applicants.findById(req.body.id, (err, app: any) => {
				app.experience = req.body.experience;
				app.availability = req.body.availability;
				app.submissionDate = new Date().toJSON().slice(0, 10);
				app.save();
				if (err) {
					res.send(err);
				}else {
					res.sendStatus(200);
				}
			});
		}
	}
	/**
	 * This function changes the application status in the databas to accepted or rejected, it also adds who handled the application.
	 * 
	 * @param req an Object with the application creators ssn, the new status of the application and who handled it.
	 * @param res on succes it will return status 200, else it will return the error.
	 */
	handleApplication(req: Request, res: Response) {
		let session = null;
		let dbString: string, testString: string;
		Applicants.db.startSession()
			.then(_session => {
				session = _session;
				session.startTransaction();
				return Applicants.findById(req.body.original._id).session(session).exec((err, application) => {
					dbString = JSON.stringify(application);
					testString = JSON.stringify(req.body.original);
				});
			})
			.then(() => {
				if (dbString === testString) {
					Applicants.findById(req.body.application.applicant, (err, app: any) => {
						app.application = {
							status: req.body.application.status,
							handledBy: req.body.application.handledBy
						}
						app.save();
						if (err) {
							session.abortTransaction();
							res.send(err);
						} else {
							session.commitTransaction()
								.then(() => res.sendStatus(200));
						}
					});
				} else {
					session.abortTransaction();
					res.sendStatus(401);
				}
			});
	}
}
