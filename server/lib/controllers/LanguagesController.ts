import * as mongoose from "mongoose";
import {Request, Response} from 'express';
import { LanguageSchema } from '../models/schemas'
import { Language } from '../models/Language';

const Languages = mongoose.model('Languages', LanguageSchema, 'Languages');

export class LanguageController {

	/**
	 * gets all languages that the page can be translated to
	 * @param req an empty request
	 * @param res the response that should be sent to the view
	 */
	getAll(req: Request, res: Response) {
		Languages.find((err, languages: Language[]) => {
			if (err) {
				res.send(err);
			} else {
				res.json(languages);
			}
		})
	}
}
