import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import { CompetenceSchema } from '../models/schemas';
import { Competence } from '../models';
import assert = require('assert');
import { Schema } from 'mongoose';


const Competences = mongoose.model('Competences', CompetenceSchema, 'Competences');

export class CompetencesController {

	/**
	 * gets all application of chosen language
	 * @param req the language
	 * @param res if it succeeded or failed
	 */
	getAll(req: Request, res: Response) {
		let comps: any;
		switch (req.params.lang) {
			case 'sv':
				Competences.find({}).select('comp_id nameSv').lean().exec((err, competences) => {
					if (err) {
						res.send(err);
					}
					comps = competences.map((x: Competence) => { return { comp_id: x.comp_id, name: x.nameSv }});
					res.json(comps);
				});
				break;
			case 'en':
				Competences.find({}).select('comp_id nameEn').lean().exec((err, competences) => {
					if (err) {
						res.send(err);
					}
					comps = competences.map((x: Competence) => { return { comp_id: x.comp_id, name: x.nameEn }});
					res.json(comps);
				});
				break;
		}
	}

	/**
	 * Add a competence to the database
	 * @param req the name of the competence eng and swe
	 * @param res if it succeeded or failed
	 */
	addCompetence(req: Request, res: Response) {
		let session = null;
		let id: number;
		let comp = <Competence> {};
		comp.nameEn = req.body['en'];
		comp.nameSv = req.body['sv'];

		Competences.db.startSession()
			.then(_session => {
				session = _session;
				session.startTransaction();
				return Competences.find({}).session(session).exec((err, competences: Competence[]) => {
					id = Math.max(...competences.map(x => x.comp_id)) + 1; // Get next id value
				});
			})
			.then(() => {
				let newCompetence = new Competences({ comp_id: id, nameSv: comp.nameSv, nameEn: comp.nameEn });
				newCompetence.save((err, competence) => {
					if (err) {
						res.send(err);
					} else {
						session.commitTransaction()
							.then(() => res.sendStatus(200));
					}
				});
			});
	}

	/**
	 * gets a competence by its id
	 * @param req the id
	 * @param res the response that should be sent to the view
	 */
	getById(req: Request, res: Response) {
		Competences.findById(req.params.id, (err, competence) => {
			if (err) {
				res.send(err);
			}
			res.json(competence);
		})
	}

	/**
	 * delete a competence by it's id
	 * @param req the id
	 * @param res the response that should be sent to the view
	 */
	deleteCompetence(req: Request, res: Response) {
		Competences.findByIdAndDelete(req.params.id, (err, competence) => {
			if (err) {
				res.send(err);
			}
			res.json(competence);
		});
	}
}