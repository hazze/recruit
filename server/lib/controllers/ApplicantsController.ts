import * as mongoose from 'mongoose';
import * as encrypt from "mongoose-encryption";
import * as passwordhash from "password-hash";
import { Request, Response } from 'express';
import { Applicant } from '../models';
import { ApplicantSchema } from '../models/schemas';
import { Middlewares } from '../authentication/middlewares';
import { type } from 'os';
import bodyParser = require('body-parser');


const encKey = process.env.ENCRYPTIONKEY;
const sigKey = process.env.SIGNINGKEY;
ApplicantSchema.plugin(encrypt, { encryptionKey: encKey, signingKey: sigKey, excludeFromEncryption: ['email', 'password', 'person_id', 'application'] });

const Applicants = mongoose.model('Applicants', ApplicantSchema, 'Applicants');
const auth = new Middlewares();


export class ApplicantsController {
	
	/**
	 * gets all application
	 * @param req an empty request
	 * @param res the response with all applicants or an error
	 */
	getAll(req: Request, res: Response) {
		Applicants.find({}, (err, applicants: Applicant[]) => {
            if (err) {
                res.send(err);
			} else {
				res.json(applicants);
			}
        });
	}

	/**
	 * gets an applicant given an id
	 * @param req an object with user data
	 * @param res the full applicant or an error
	 */
	getById(req: Request, res: Response) {
		Applicants.findOne({ 'email': req.params.id }, (err, applicant) => {
			if (err) {
				res.send(err);
			}
			res.json(applicant);
		})
	}

	/**
	 * a check if the database is working
	 * @param req an empty request
	 * @param res ok if the database is working, else an error
	 */
	databaseCheck(req: Request, res: Response) {
		let status = mongoose.connection.readyState;
		if (status == 1) {
			res.sendStatus(200);
		} else {
			res.sendStatus(400)
		}
	}

	/**
	 * adds an applicant user to the database
	 * @param req the user data that sould be added to the database
	 * @param res if it succeeded or failed
	 */
	addApplicant(req: Request, res: Response) {
		const reqBdy: any = req;
		reqBdy.check('ssn').isNumeric().len({ max: 10 });
		reqBdy.check('email').isEmail();
		reqBdy.check('name').isAlpha();
		reqBdy.check('surname').isAlpha();
		reqBdy.check('password').notEmpty();
		let errors = reqBdy.validationErrors();

		if(errors) {
			res.sendStatus(406);
		}
		else {
			let session = null;
			let id: number;
			Applicants.db.startSession()
				.then(_session => {
					session = _session;
					session.startTransaction();
					return Applicants.find({}).session(session).exec((err, applicants: Applicant[]) => {
						id = Math.max(...applicants.map(x => x.person_id)) + 1; // Get next id value
						req.body.person_id = id; // Get next id value
					});
				})
				.then(() => {
					req.body.password = passwordhash.generate(req.body.password);
					let newApplicant = new Applicants(req.body);
					newApplicant.save((err, applicant) => {
					if (err) {
						res.send(err);
					} else {
							session.commitTransaction()
								.then(() => res.sendStatus(200));
						}
					});
				});
		}
	}

	/**
	 * removes an applicant from the database
	 * @param req the user data of the user that should be deleted
	 * @param res if it succeeded or failed
	 */
	deleteApplicant(req: Request, res: Response) {
		Applicants.findByIdAndDelete(req.params.id, (err, applicant) => {
			if (err) {
				res.send(err);
			}
			res.json(applicant);
		});
	}

	/**
	 * logging i a applicant user and sets session data
	 * @param req email and password
	 * @param res if it succeeded or failed
	 */
	login(req: Request, res: Response) {
		const reqBdy: any = req;
		reqBdy.check('username').isEmail();
		reqBdy.check('password').notEmpty();
		let errors = reqBdy.validationErrors();

		if(errors) {
			res.sendStatus(400);
		}
		else {
			Applicants.findOne({ 'email': req.body.username }, (err, applicant: Applicant) => {
				if (err) {
					res.status(404).send(err);
				}
				if (applicant && passwordhash.verify(req.body.password, applicant.password)) {
					let responseJson = {
						success: true,
						admin: false,
						username: applicant.email,
						token: auth.createJWT_MW({
							sessionData: {
								user: applicant.email,
								person_id: applicant.person_id,
							},
							maxAge: 3600
						})
					};
					res.json(responseJson);
					res.send();
				}
				else {
					res.status(403).send();
				}
			});
		}
		
		
	}
}