import * as mongoose from 'mongoose';
import * as passwordhash from 'password-hash';
import { Request, Response } from 'express';
import { RecruitSchema } from '../models/schemas';
import { Recruit } from '../models';
import { Middlewares } from '../authentication/middlewares';


const Recruits = mongoose.model('Recruits', RecruitSchema, 'Recruits');
const auth = new Middlewares();

export class RecruitsController {

	/**
	 * loggin for a recruit user
	 * @param req username and password
	 * @param res the response that should be sent to the view
	 */
	login(req: Request, res: Response) {
		const reqBdy: any = req;
		reqBdy.check('username').notEmpty();
		reqBdy.check('password').notEmpty();
		let errors = reqBdy.validationErrors();

		if(errors) {
			res.sendStatus(400);
		}
		else {
		
			Recruits.findOne({ 'username': req.body.username }, (err, recruit: Recruit) => {
				if (err) {
					res.status(404).send(err);
				}
				if (recruit && passwordhash.verify(req.body.password, recruit.password)) {
					let responseJson = {
						success: true,
						admin: true,
						username: recruit.username,
						token: auth.createJWT_MW({
							sessionData: {
								user: recruit.username,
								person_id: recruit.person_id,
							},
							maxAge: 3600
						})
					};
					res.json(responseJson);
					res.send();
				}
				else {
					res.status(403).send();
				}
			});
		}
	}
}