import { Middlewares } from '../authentication/middlewares';
import { NextFunction } from 'connect';
import { ApplicantsController } from '../controllers/ApplicantsController';
import { CompetencesController } from "../controllers/CompetencesController";
import {ApplicationController} from "../controllers/ApplicationController";
import { RecruitsController } from '../controllers/RecruitsController';
import { LanguageController } from '../controllers/LanguagesController';

const auth = new Middlewares();
const { check } = require('express-validator/check');

var unless = function(middleware, ...paths) {
	return function(req: Request, res: Response, next: NextFunction) {
	  	const pathCheck = paths.some(path => path === req.url);
	  	pathCheck ? next() : middleware(req, res, next);
	};
  };

export class Routes {   
	applicantsController: ApplicantsController = new ApplicantsController();
	recruitsController: RecruitsController = new RecruitsController();
	competencesController: CompetencesController = new CompetencesController();
	applicationController: ApplicationController = new ApplicationController();
	languageController: LanguageController = new LanguageController();


	routes(app: any): void {
		app.use(unless(auth.verifyJWT_MW, '/api/applicants/login', '/api/applicants/add', '/api/recruits/login', '/api/languages/all', '/api/applicants/databaseCheck'));


		app.route('/api/applicants/all')
			.get(this.applicantsController.getAll);

		app.route('/api/applicants/add')
			.post( this.applicantsController.addApplicant);

		app.route('/api/applicants/byId/:id')
			.get(this.applicantsController.getById)
			.delete(this.applicantsController.deleteApplicant);

		app.route('/api/applicants/login')
			.post(this.applicantsController.login);

		app.route('/api/recruits/login')
			.post(this.recruitsController.login);

		app.route('/api/competences/all/:lang')
			.get(this.competencesController.getAll);

		app.route('/api/competences/add/')
			.post(this.competencesController.addCompetence);

		app.route('/api/competences/byId/:id')
			.get(this.competencesController.getById)
			.delete(this.competencesController.deleteCompetence);

		app.route('/api/application/add')
			.post(this.applicationController.addApplication);

		app.route('/api/application/handle')
			.post(this.applicationController.handleApplication);

		app.route('/api/languages/all')
			.get(this.languageController.getAll);

			app.route('/api/applicants/databaseCheck')
			.get(this.applicantsController.databaseCheck);

		app.route(/.*/, (req, res) => {
			res.sendFile('./../../dist/public/index.html')
		})
    }
}
