# Recruit

## Client

The client is built in VueJs using components for diffrent pages, to create a component you must add a folder with the new component name in Clinet/recruit/src/app/components/. Inside the folder you need a .vue file where de HTML design should be, in our case we use a material design library called vuetify for the design. The folder also need a .ts(TypeScript) file where all the scripts for the component should be. When you create a new component you have to add it in client/recruit/src/app/components/recruit/recruit.ts.


To send of recive data from the backend you have to create a function in client/recruit/src/services/data-servive.ts which talk to the server using axios. This funktion should be called from the .ts file in the component.
### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Lints and fixes files
```
npm run lint
```


## Server

The backend is also written in TypeScript using ExpressJs framework. When the client makes a call to the server you will have to redirect it in server/lib/routes/routes.ts into the correct funktion at server/lib/controllers.

For database we are using Mongdb with Mongoos to get built-in type casting, validation, query building, business logic.

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run dev
```

#### Compiles and minifies for production
```
npm run prod
```

## API Reference
```
GET /applicants/all
```
Response
```Typescript
<Applicant> [{
	_id: string;
	person_id: number;
	name: string;
	surname: string;
	ssn: string;
	email: string;
	password: string;
	experience: Experience[] | null;
	availability: Availability[] | null;
	application: Application | null;
	submissionDate: string;
}]
```
```
GET /competences/all/:lang
```
Response
```Typescript
<Competence> [{
    comp_id: number;
    name: string;
}]

```
```
GET /applicants/byId/:id
```
Response
```Typescript
<Applicant> {
	_id: string;
	person_id: number;
	name: string;
	surname: string;
	ssn: string;
	email: string;
	password: string;
	experience: Experience[] | null;
	availability: Availability[] | null;
	application: Application | null;
	submissionDate: string;
}
```
```
POST /applicants/add
```
Parameter
```Typescript
<Applicant> {
	_id: string;
	person_id: number;
	name: string;
	surname: string;
	ssn: string;
	email: string;
	password: string;
	experience: Experience[] | null;
	availability: Availability[] | null;
	application: Application | null;
	submissionDate: string;
}
```
Response
```Typescript
<Applicant> {
	_id: string;
	person_id: number;
	name: string;
	surname: string;
	ssn: string;
	email: string;
	password: string;
	experience: Experience[] | null;
	availability: Availability[] | null;
	application: Application | null;
	submissionDate: string;
}
```
```
POST /application/add
```
Parameter
```Typescript
<any> {
	id: string;
	availability: Availability[];
	experience: Experience[];
}
```
Response
```Typescript
<any> {}
```
```
POST /competences/add
```
Parameter
```Typescript
<CompetenceDTO> {
	[key: string]: any;
}
```
Response
```Typescript
<any> {}
```
```
POST /application/handle
```
Parameters
```Typescript
<any> {
	application: <any> {
		handledBy: string;
		status: string;
		applicant: string
	},
	original: <Applicant> {
		_id: string;
		person_id: number;
		name: string;
		surname: string;
		ssn: string;
		email: string;
		password: string;
		experience: Experience[] | null;
		availability: Availability[] | null;
		application: Application | null;
		submissionDate: string;
	}
}
```
Response
```Typescript
<any> {}
```
```
GET /languages/all
```
Response
```Typescript
<Language> {
	name: string | TranslateResult;
	code: string;
}
```
```
GET /applicants/databaseCheck
```
Response
```Typescript
<any> {}
```
#### Authentication
```
POST /recruits/login
```
Parameters
```Typescript
{
	method: 'POST',
	headers: { 'Content-Type': 'application/json' },
	body: { username: string; password: string }
}
```
Response
```Typescript
{
	success: boolean;
	admin: boolean;
	username: string;
	token: string;
}
```
```
POST /applicants/login
```
Parameters
```Typescript
{
	method: 'POST',
	headers: { 'Content-Type': 'application/json' },
	body: { username: string; password: string }
}
```
Response
```Typescript
{
	success: boolean;
	admin: boolean;
	username: string;
	token: string;
}
```
