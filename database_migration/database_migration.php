<?php

    $con = mysqli_connect("localhost","jocke","jocke97","recruit");

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

  $sql    = "select t1.person_id, 
  t1.name, t1.surname, 
  t1.ssn, t1.email, t1.password, 
  GROUP_CONCAT(t2.experiences) as experiences, GROUP_CONCAT(t2.availability) as availability    
  from person t1 left join    (select t3.person_id, GROUP_CONCAT(distinct(t3.exp))as experiences, 
  GROUP_CONCAT(distinct(t4.av)) as availability   
  from (select person_id, CONCAT('{', competence.name,',', years_of_experience,'}')as exp   
  from competence, competence_profile 
  WHERE   competence.competence_id = competence_profile.competence_id) t3 
  left join   (select person_id, CONCAT('{', from_date ,',', to_date ,'}')as av   from availability) t4   
  on t4.person_id = t3.person_id group by t3.person_id)
   t2 on t2.person_id = t1.person_id where role_id = 2 group by t1.person_id;";
  $result = mysqli_query($con, $sql);
  $rows1 = array();
  if (mysqli_num_rows($result) > 0) {
      
      while ($row = mysqli_fetch_assoc($result)) {
  
          $rows1[] = $row;
          
      }
  
      print json_encode($rows1);
  } else {
      echo "no results found";
  }



?>

